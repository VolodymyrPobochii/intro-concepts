package intro.concepts

import grails.test.mixin.integration.Integration
import grails.transaction.Rollback

// tag::PersonRestSpec[]
@Integration
@Rollback // <1>
class PersonRestSpec extends RestSpec { // <2>
    void "Create a person"() { // <3>
        setup: "Create a person via GORM"
        new Person(firstName: "Eric", lastName: "Helgeson").save()

        when: "Create a person via the API"
        def response = rest.post("${baseUrl}/example/save") { // <4>
            accept('application/json')
            json('{"firstName":"Foo","lastName":"Bar"}')
        }

        then: "API call success"
        response.status == 200
        response.json.firstName == "Foo"

        and: "there is 2 people"
        Person.count == 2
    }
}
// tag::PersonRestSpec[]

