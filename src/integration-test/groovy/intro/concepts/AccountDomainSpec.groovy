package intro.concepts

import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import spock.lang.Specification


// tag::AccountDomainIntSpec[]
@Integration
@Rollback
class AccountDomainSpec extends Specification {
    void "An account with name and balance saves"() {
        expect:
        Account.count == 0 // <1>
        new Account(name:"Savings",balance: 1_00).save() // <2>
        Account.count == 1 // <3>
    }

    void "The account created previously is rolled back"() {
        expect:
        Account.count == 0 // <4>
    }
}
// end::AccountDomainIntSpec[]
