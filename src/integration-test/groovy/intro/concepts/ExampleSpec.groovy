package intro.concepts

import grails.test.mixin.integration.Integration
import grails.transaction.*

import spock.lang.*
import geb.spock.*

/**
 * See http://www.gebish.org/manual/current/ for more instructions
 */
// tag::ExampleSpecIntSpec[]
@Integration
@Rollback // <1>
class ExampleSpec extends GebSpec { // <2>
    void "Test Example index page"() {
        when:"The example page is loaded"
        go '/example/index' // <3>

        then:"The body is correct"
        $("body").text() == "Model: Oliver 123" // <4>
    }
}
// end::ExampleSpecIntSpec[]
