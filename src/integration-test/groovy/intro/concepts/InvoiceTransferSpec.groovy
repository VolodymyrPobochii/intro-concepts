package intro.concepts


import grails.test.mixin.integration.Integration
import grails.transaction.*
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.*

// tag::InvoiceTransferSpecIntSpec[]
@Integration
@Rollback
class InvoiceTransferSpec extends Specification {
    @Autowired AccountService accountService // <1>

    void "Transfer"() {
        setup:
        def from = new Account(name: "From", balance: 1_00).save()
        def to = new Account(name: "To", balance: 1_00).save()

        when:
        accountService.transfer(from.id, to.id, 50) // <2>

        then:
        from.balance == 50
        to.balance   == 1_50

        and:
        Account.count == 2
    }

    void "didn't pollute"() {
        expect:
        Account.count == 0  // <3>
    }
    // end::InvoiceTransferSpecIntSpec[]
}
