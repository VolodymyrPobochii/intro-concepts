package intro.concepts

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(BookTagLib)
class BookTagLibSpec extends Specification {

    def setup() {
        // Ensure en_US is set for locale for currency format.
        request.addPreferredLocale(Locale.US)
    }

    def cleanup() {}

    // tag::testPriceTag[]
    void "test price"() {
        expect:
        tagLib.price(price: 1_00).toString() == "<div class='price' data-price='100'>\$1.00</div>"
        tagLib.price(price: 1_10).toString() == "<div class='price' data-price='110'>\$1.10</div>"
        tagLib.price(price: 1_11).toString() == "<div class='price' data-price='111'>\$1.11</div>"
        tagLib.price(price: 1_000_10).toString() == "<div class='price' data-price='100010'>\$1,000.10</div>"

    }
    // end::testPriceTag[]

    // tag::testDisplayTag[]
    void "test display"() {
        given:"A book object"
        def book = [id: 1, title: "foo", price: 1_00]

        expect:
        tagLib.display(book:book).toString() == """<div class='book'>
    <h2>foo</h2>
    <div class='price' data-price='100'>\$1.00</div> <!-- 1 -->
    <a href="/example/tagLibs/1">BUY</a>
</div>"""

    }
    // tag::testDisplayTag[]
}
