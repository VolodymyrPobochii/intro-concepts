package intro.concepts

import grails.test.mixin.TestFor
import grails.test.hibernate.HibernateSpec

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Person)
class PersonSpec extends HibernateSpec {

    def setup() {
      // Generate 100 people and save them
      (1..100).each { number ->
        new Person(
          firstName: "Eric",
          lastName: number,
          active: (number % 2 == 0) // Half will be active
          ).save()
      }

      new Person(
        firstName: "Eric",
        lastName: "Helgeson",
        active: true
        ).save()
    }

    def cleanup() { }

    void "Dynamic Finders Examples"() {
        expect:"Examples to be valid"
        // tag::PersonDynamicFinders[]
        // Find all people that name start with Eric
        Person.findAllByFirstNameIlike("Eric%")

        // Find all inactive People
        Person.findAllByActive(false) // <1>

        // Paginate through all active Persons, 10 at a time.
        Person.findAllByActive(true, [max: 10, sort: "lastName", order: "desc", offset: 20]) // <2>

        // Find all Active People named Eric
        Person.findAllByActiveAndFirstName(true, "Eric")

        // Find ONE person with the name Eric Helgeson
        Person.findByFirstNameAndLastName("Eric", "Helgeson")
        // end::PersonDynamicFinders[]
    }

    void "Where queries Examples"() {
        // tag::PersonWhere[]
        given:"A first name Eric"
        def firstName = "Eric"

        expect: "A list of all people with the first name Eric"
        Person.where {
            firstName == firstName
        }.count() == 101

        and: "Paginate through People, if a first name is given, search by it."
        Person.where {
            if(firstName) {
                firstName =~ firstName
            }
        }.max(10).offset(100).list().size() == 1
        // end::PersonWhere[]
    }

    void "Criteria Examples"() {
      expect:"Examples to be valid"
      def firstName = "Eric"
      // tag::PersonCriteria[]
      // A list of all people with the first name Eric
      Person.withCriteria {
        eq('firstName', 'Eric')
      }

      // Paginate through People, if a first name is given, search by it.
      Person.withCriteria (max:10, offset:100) {
        if(firstName) { // <1>
          ilike('firstName', firstName)
        }
      }
      // end::PersonCriteria[]
    }

//    @Ignore("findAll can not be used in unit tests")
    void "HQL Examples"() {
      expect:"Examples to be valid"
      // tag::PersonHQL[]
      Person.findAll("from Person as p where p.firstName like 'Eric%'")
      // end::PersonHQL[]
    }
}
