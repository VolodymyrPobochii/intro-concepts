package intro.concepts

import grails.test.hibernate.HibernateSpec
import grails.test.mixin.TestFor
import spock.lang.Ignore

@TestFor(AccountService)
class AccountServiceSpec extends HibernateSpec {

    def setup() {
    }

    def cleanup() {
    }

    void "Transfer balance"() {
        setup: "Two accounts with 1 usd"
        def to = new Account(name: "to", balance: 1_00).save()
        def from = new Account(name: "from", balance: 1_00).save()

        when: "Transfer 50cents"
        service.transfer(from.id, to.id, 50)

        then: "Balance transfer successful"
        to.balance == 1_50
        from.balance == 50
    }

    @Ignore("This is slightly flaky, need to figure out why")
    // tag::AccountServiceEventSpec[]
    void "transferEventsExample large transfer"() {
        setup: "Two accounts with large balances"
        def to = new Account(name: "to", balance: 19_999_99).save()
        def from = new Account(name: "from", balance: 19_999_99).save()

        when: "Send large Transfer"
        service.transferEventsExample(from.id, to.id, 11_000_01)

        then: "transfer.big notification is sent"
        1 * service.notify('intro.concepts.transfer.big', _) // <1>
    }

    void "transferEventsExample small transfer"() {
        setup: "Two accounts with large balances"
        def to = new Account(name: "to", balance: 19_999_99).save()
        def from = new Account(name: "from", balance: 19_999_99).save()

        when: "Send large Transfer"
        service.transferEventsExample(from.id, to.id, 50)

        then: "transfer.big notification is sent"
        0 * service.notify('intro.concepts.transfer.big', _) // <2>
    }
    // end::AccountServiceEventSpec[]
}
