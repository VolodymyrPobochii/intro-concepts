package intro.concepts

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(Account)
class AccountSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "domain validates"() {
        given:
        domain.name = "foo"
        domain.balance = 1

        expect:
        domain.validate()
    }
    void "domain does not validates"() {
        expect:
        !domain.validate()
    }
}
