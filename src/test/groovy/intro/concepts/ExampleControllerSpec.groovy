package intro.concepts

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

import static java.net.HttpURLConnection.*


@TestFor(ExampleController)
class ExampleControllerSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "index json works"() {
        given: "JSON Header"
        request.addHeader('Accept', 'application/json')

        when: "index action is called"
        controller.index()

        then: "Correct response"
        response.status == HTTP_OK
        response.json.name == "Oliver"
        response.json.id == 123
    }

    void "index xml works"() {
        given: "XML header"
        request.addHeader('Accept', 'application/xml')

        when: "index action is called"
        controller.index()

        then: "Correct response"
        response.status == HTTP_OK
        response.text == '<?xml version="1.0" encoding="UTF-8"?><map><entry key="name">Oliver</entry><entry key="id">123</entry></map>'
    }

    @Unroll
    void "hello works"() {
        given: "JSON Header"
        request.addHeader('Accept', 'application/json')
        params.name = name

        when: "index action is called"
        controller.hello()

        then: "Correct response"
        response.status == HTTP_OK
        response.json.greeting == greeting

        where:
        name     | greeting
        null     | "Hello world"
        "Eric"   | "Hello Eric"
    }
}
