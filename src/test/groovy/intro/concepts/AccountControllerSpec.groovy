package intro.concepts

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(AccountController)
class AccountControllerSpec extends Specification {
    // Logic is covered in AccountServiceSpec
}
