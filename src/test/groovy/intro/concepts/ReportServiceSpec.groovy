package intro.concepts

import grails.test.hibernate.HibernateSpec
import grails.test.mixin.Mock
import grails.test.mixin.TestFor

@TestFor(ReportService)
@Mock(Account)
class ReportServiceSpec extends HibernateSpec {

    def setup() {
    }

    def cleanup() {
    }

    void "dailyBalanceReport"() {
        setup:
        def account = new Account(name: "123", balance: 1_000_000_01).save(flush:true)

        when:
        service.dailyBalanceReport()

        then:
        account.balance == 1_000_000_02
    }
}
