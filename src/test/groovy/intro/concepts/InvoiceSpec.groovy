package intro.concepts

import grails.test.mixin.TestFor
import grails.test.mixin.Mock
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Invoice)
@Mock([Invoice,Item])
class InvoiceSpec extends Specification {
    def testInvoice // Just a holder to make examples look better.

    def setup() {
      // tag::InvoiceTest[]
      def invoice = new Invoice(name: "Invoice 1").save()

      def widget = new Item(name: "Widget", amount: 1000).save()
      def bar = new Item(name: "Bar", amount: 500).save()

      invoice.addToItems(widget) // <1>
      invoice.addToItems(bar)
      // end::InvoiceTest[]

      testInvoice = invoice
    }

    def cleanup() { }

    void "Totals"() {
        given:
        def invoice = testInvoice

        expect:"To return total"
        invoice.total == 1500
    }
}
