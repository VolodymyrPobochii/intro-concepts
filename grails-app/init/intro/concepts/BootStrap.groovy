package intro.concepts

import grails.util.Environment


class BootStrap {
    def init = { servletContext ->
        // Don't pollute our Integration tests with data.
        if(Environment.current != Environment.TEST) {
            // tag::createPerson[]
            def person = new Person(firstName: "Eric",
                    lastName: "Helgeson") // <1>
            person.firstName = "Bob"                       // <2>
            assert !person.active                         // <3>
            person.save(flush: true)                        // <4>
            Person.findByFirstName("Bob")                  // <5>
            // end::createPerson[]

            new Account(name: "1", balance: 10_000_00).save(flush: true)
            new Account(name: "2", balance: 10_000_00).save(flush: true)
        }
    }


    def destroy = {
    }
}
