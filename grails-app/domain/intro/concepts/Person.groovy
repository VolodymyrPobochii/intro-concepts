package intro.concepts

class Person {
    // tag::PersonProperties[]
    String firstName        // <1>
    String lastName
    Boolean active = false  // <2>
    // end::PersonProperties[]
    // tag::PersonPropertiesEmail[]
    String emailAddress      // <1>
    // end::PersonPropertiesEmail[]


    static constraints = { // <3>
      // tag::PersonConstraints[]
      emailAddress nullable: true, email: true, unique: true // <3>
      // end::PersonConstraints[]
    }

    // tag::toString[]
    String toString() {
      firstName + " " + lastName
    }
    // end::toString[]

    // To illustrate command objects. We will use Spring-Security to manage users.
    def changePass(String password) {
      println "Changed password!"
      true
    }
}
