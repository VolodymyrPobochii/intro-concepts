package intro.concepts

// tag::AccountClass[]
class Account {
    String name
    Integer balance  // <1>

    static constraints = { }
}
// end::AccountClass[]
