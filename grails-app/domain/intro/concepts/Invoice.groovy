package intro.concepts

// tag::InvoiceClass[]
class Invoice {
    String name
    // <1>
    static hasMany = [items: Item]

    static constraints = { }
    // end::InvoiceClass[]

    // tag::InvoiceClassGetTotal[]
    def getTotal() {
      items.sum { it.amount }
    }
    // end::InvoiceClassGetTotal[]
}
