package intro.concepts

import grails.transaction.Transactional
import org.springframework.transaction.annotation.Propagation

class ReportService {

    @Transactional(readOnly = true)
    def dailyBalanceReport() { // <1>
        Account.where {
            balance > 1_000_000_00
        }.list().each { Account account ->
            giveReward(account)
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW) // <2>
    def giveReward(Account account) {
        account.balance += 1
        account.save()
    }
}