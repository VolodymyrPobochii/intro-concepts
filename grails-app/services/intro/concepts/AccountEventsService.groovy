package intro.concepts

import reactor.spring.context.annotation.Consumer
import reactor.spring.context.annotation.Selector

@Consumer // <1>
class AccountEventsService {

    @Selector('intro.concepts.transfer.big') // <2>
    void sendEmail(map) { // <3>
        println "There was a large transfer, sending email to bigtransfer@irs.gov: $map.fromId -> $map.toId : $map.amount"
    }

    @Selector('intro.concepts.transfer.big') // <4>
    void logAPICall(map) {
        println "There was a large transfer, Calling External API: $map.fromId -> $map.toId : $map.amount"
    }

    @Selector('intro.concepts.transfer.failed') // <3>
    void transferFailed(map) {
        println "A transfer failed, sending email to ops@example.com: $map.fromId -> $map.toId : $map.amount"
    }
}
