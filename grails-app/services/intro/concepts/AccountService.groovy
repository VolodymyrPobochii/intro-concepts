// tag::AccountService[]
package intro.concepts

import grails.transaction.Transactional

@Transactional // <1>
class AccountService {

    def transfer(Long fromId, Long toId, Integer amount) {
        Boolean transfer = false
        Account from = Account.get(fromId)
        Account to = Account.get(toId)

        from.balance -=  amount // <2>
        to.balance += amount

        if(from.save() && to.save()) {  // <3>
            transfer = true
        } else {
            transactionStatus.setRollbackOnly()  // <4>
        }

        return [transfer: transfer]
    }

    @Transactional(readOnly = true)
    def get(Long id) {
        return Account.get(id)
    }
// end::AccountService[]

    // Example for events
    def transferEventsExample(Long fromId, Long toId, Integer amount) {
        // tag::AccountServiceEvent[]
        Boolean transfer = false
        Account from = Account.get(fromId)
        Account to = Account.get(toId)

        from.balance -=  amount
        to.balance += amount

        if(from.save() && to.save()) {
            transfer = true
            if(amount > 9_999_99) {
                def map = [fromId:fromId, toId:toId, amount:amount]
                notify('intro.concepts.transfer.big', map) // <1>
            }
        } else {
            def map = [fromId:fromId, toId:toId, amount:amount]
            notify('intro.concepts.transfer.failed', map) // <1>
            transactionStatus.setRollbackOnly()
        }

        return [transfer: transfer]
        // end::AccountServiceEvent[]
    }

}